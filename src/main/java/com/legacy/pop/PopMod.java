package com.legacy.pop;

import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.fml.ModContainer;
import net.neoforged.fml.common.Mod;
import net.neoforged.fml.config.ModConfig;
import net.neoforged.fml.loading.FMLEnvironment;

@Mod(PopMod.MODID)
public class PopMod
{
	public static final String MODID = "pop";

	public PopMod(IEventBus modBus, ModContainer modContainer)
	{
		if (FMLEnvironment.dist == Dist.CLIENT)
			modContainer.registerConfig(ModConfig.Type.CLIENT, PopConfig.CLIENT_SPEC);
	}
}