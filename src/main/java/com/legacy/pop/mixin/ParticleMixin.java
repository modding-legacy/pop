package com.legacy.pop.mixin;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.legacy.pop.hooks.PopHooks;

import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.particle.Particle;

@Mixin(Particle.class)
public class ParticleMixin
{
	@Shadow
	@Final
	protected ClientLevel level;

	@Shadow
	protected double x;

	@Shadow
	protected double y;

	@Shadow
	protected double z;

	@Inject(at = @At("HEAD"), method = "remove()V")
	private void remove(CallbackInfo callback)
	{
		PopHooks.onParticleRemove((Particle) (Object) this, this.level, this.x, this.y, this.z);
	}
}