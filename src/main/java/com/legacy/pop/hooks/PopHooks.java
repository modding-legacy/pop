package com.legacy.pop.hooks;

import com.legacy.pop.PopConfig;

import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.particle.BubbleColumnUpParticle;
import net.minecraft.client.particle.BubbleParticle;
import net.minecraft.client.particle.Particle;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;

public class PopHooks
{
	public static void onParticleRemove(Particle particle, ClientLevel level, double x, double y, double z)
	{
		boolean normal = particle instanceof BubbleParticle;
		boolean column = particle instanceof BubbleColumnUpParticle;

		if (normal || column)
		{
			if (normal && PopConfig.CLIENT.splashesPlaySound() || column && PopConfig.CLIENT.columnsPlaySound())
				level.playLocalSound(x, y, z, SoundEvents.BUBBLE_COLUMN_BUBBLE_POP, SoundSource.BLOCKS, 0.3F + (level.random.nextFloat() * 0.2F), 1.0F, false);

			level.addParticle(ParticleTypes.BUBBLE_POP, x, y, z, 0, 0, 0);
		}
	}
}
