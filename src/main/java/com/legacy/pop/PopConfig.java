package com.legacy.pop;

import org.apache.commons.lang3.tuple.Pair;

import net.neoforged.neoforge.common.ModConfigSpec;
import net.neoforged.neoforge.common.ModConfigSpec.ConfigValue;

public class PopConfig
{
	public static final ModConfigSpec CLIENT_SPEC;
	public static final ClientConfig CLIENT;

	static
	{
		{
			Pair<ClientConfig, ModConfigSpec> pair = new ModConfigSpec.Builder().configure(ClientConfig::new);
			CLIENT = pair.getLeft();
			CLIENT_SPEC = pair.getRight();
		}
	}

	public static class ClientConfig
	{
		private final ConfigValue<Boolean> columnsPlaySound;
		private final ConfigValue<Boolean> splashesPlaySound;

		public ClientConfig(ModConfigSpec.Builder builder)
		{
			builder.comment("Pop! Client-side Configuration").push("client");

			columnsPlaySound = builder.comment("Allows bubbles from bubble columns to play popping noises.").define("columnsPlaySound", false);
			splashesPlaySound = builder.comment("Allows normal bubbles to play popping noises.").define("splashesPlaySound", true);
			builder.pop();
		}

		public boolean columnsPlaySound()
		{
			return columnsPlaySound.get();
		}

		public boolean splashesPlaySound()
		{
			return splashesPlaySound.get();
		}
	}
}